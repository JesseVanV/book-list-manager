import React, { Component } from 'react';
import { connect } from 'react-redux';

// 1. set up the component/container
class BookDetail extends Component {
  render () {
    //If there is no 'active book' yet, book is still undefined, so prompt user to select an active book
    if(!this.props.book) {
      return <div>Select a book on the left first</div>
    }
    return (
      <div>
      <h3>Details for</h3>
      <p>{ this.props.book.title }</p>
      <p>Pages: { this.props.book.pages }</p>
      </div>
    );
  }
}

// 2. map the state to the container's props
function mapStateToProps(state) {
  return {
    book: state.activeBook
  };
}

// 3. connect the state props to the container with Redux
export default connect(mapStateToProps)(BookDetail);
