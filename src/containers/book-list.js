import React, { Component } from 'react';
import { connect } from 'react-redux';
import { selectBook } from '../actions/index';
import { bindActionCreators } from 'redux';

class BookList extends Component {  //this is a container, or a componenet connected to Redux

  renderList() {
    return this.props.books.map((book) => {
      return (
        <li
          key={ book.title }
          onClick={ () => this.props.selectBook(book) }
          className="list-group-item">{ book.title }
        </li>
      )
    });
  }

    render() {
      return (
        <ul className="list-group col-sm-4 book-list">
        { this.renderList() }</ul>
      )
    }
}

// state(s) returned with this function will be props on container
function mapStateToProps(state) {
  // this will return to BookList as props from state, connecting React and Redux
  return {
    books: state.books
  };
}

// action(s) returned from this function will be props on 'BookList'
function mapDispatchToProps(dispatch) {
  // when 'selectBook' is called, result is passed to all reducers through 'dispatch'
  return bindActionCreators({ selectBook: selectBook }, dispatch)
}

// Promotes BookList to a container, passes props and actions to container
export default connect(mapStateToProps, mapDispatchToProps)(BookList);
