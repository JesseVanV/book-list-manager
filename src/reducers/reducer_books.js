export default function () {
  return [
    { 'title': 'Javascript the good parts', pages: 105 },
    { 'title': 'Harry Potter', pages: 55 },
    { 'title': 'Dark Tower', pages: 655 },
    { 'title': 'PHP pandas', pages: 35 }
  ]
}
