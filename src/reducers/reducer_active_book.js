
// all reducers receive 2 args, state is only the state this reducer is responsible before
// state cannot be undefined in react, so set it to null if not passed
export default function(state = null, action) {
  switch(action.type) {
    case 'BOOK_SELECTED':
      return action.payload;
  }
  return state;
}
