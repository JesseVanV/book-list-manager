// ActionCreator returns an object with a type property
export function selectBook(book) {
  return {
    type: 'BOOK_SELECTED',  //type is always expressed as uppercase
    payload: book
  };
}
